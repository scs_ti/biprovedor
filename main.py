from functions import *
from querys import *
from sqlalchemy.types import INTEGER
from datetime import datetime

# %% PARAMENTROS DE CONEXÕES AOS BANCO DE DADOS
user_dw,  psw_dw, host_dw = get_paramentros_con('prd_db_provedor.db')
user_oracle,  psw_oracle, host_oracle = get_paramentros_con('prdme.db')

# %%CONEXÃO DO BANCO MYSQL ONDE ESTA O DW
engine_dw = get_engine_dw()

# Total de dias a atualizar
dias_despesas = 2
dias_receitas = 6

# INÍCIO DO PROGRAMA

## CARGA NA TABELA DIM_BENEFICIARIO
### Pegando todos os beneficiários do banco de produção
with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as connection:
    df_beneficiario_atualizado = pd.read_sql(sql_Dim_beneficiario, connection)

#Encontrando novos beneficiários a serem cadastrados
df_ben_banco_dw = pd.read_sql('SELECT t.* FROM dim_beneficiario t', con=engine_dw)
df_novos_beneficiarios = df_beneficiario_atualizado[~df_beneficiario_atualizado['cd_matricula'].isin(df_ben_banco_dw['cd_matricula'])]
if len(df_novos_beneficiarios) == 0:
    print('Nenhum Beneficiário novo encontrado.')
else:
    df_novos_beneficiarios.to_sql('dim_beneficiario', con=engine_dw, if_exists='append', index=False)
    print(len(df_novos_beneficiarios), 'Benficiário(os) novo(os) Cadastrados')

# CARGA NA TABELA DIM_PRESTADOR
last_id_prestador = get_last_id('dim_prestador', 'cd_prestador', engine_dw).values[0][0]
if last_id_prestador is None:
    sql_dim_prestador = sql_dim_prestador.format('')
else:
    sql_dim_prestador = sql_dim_prestador.format('WHERE P.CD_PRESTADOR > {}'.format(last_id_prestador))

with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as connection:
    df_prestador_append = pd.read_sql(sql_dim_prestador, connection)

if df_prestador_append.empty:
    print('Nenhum prestador encontrado')
else:
    print(len(df_prestador_append), ' prestador(es) encontrado.')
    df_prestador_append.to_sql('dim_prestador', con=engine_dw, if_exists='append', index=False)
    print(len(df_prestador_append), ' prestador(es) cadastrado com sucesso!')


# CARGA NA TABELA FATO_RECEITAS
last_id_fato_receita = get_last_id('fato_receitas','cd_contrato',engine_dw).values[0][0]
if last_id_fato_receita is None:
    print('Nenhum registro na fato_receita.')
    sql_fato_receitas = sql_fato_receitas.format('')
    print('pegando receitas do banco de produção')
    with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as connection:
        df_fato_receitas = pd.read_sql(sql_fato_receitas, connection)
else:
    print('já existe dados na fato receitas.')
    print('Pegando receitas do banco de produção')
    sql_fato_receitas = sql_fato_receitas.format("AND M.NR_ANO || M.NR_MES >= to_char(last_day(add_months(sysdate, -{})) + 1,'yyyymm')".format(dias_receitas))
    print('Deletando registros dos ultimos {} meses da fato receitas'.format(dias_receitas))
    with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as connection:
        df_fato_receitas = pd.read_sql(sql_fato_receitas, connection)
    with engine_dw.connect() as con:
        result = con.execute(
            "DELETE FROM fato_receitas d where d.cd_tempo >= date_format(last_day(curdate() - interval {} month) + interval 1 day,'%%Y%%m')".format(dias_receitas))
    print('deletado com sucesso.')



print('executando o carregamento dos dados na tabela fato_receitas...{} Registros.'.format(len(df_fato_receitas)))
df_fato_receitas[df_fato_receitas['CD_TEMPO'] >='2018'].to_sql('fato_receitas', con=engine_dw, if_exists='append', index=False, dtype={"cd_tempo": INTEGER})
print('carregamento tabela fato_receitas finalizado.')


#   CARGA NA TABELA FATO_DESPESAS

print('Pegando as despesas do banco de produção.')
with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as connection:
    df_fato_despesa = pd.read_sql(sql_fato_despesas.format(dias_despesas, dias_despesas, dias_despesas), connection)

print('deletando registros dos ultimos {} meses da fato despesas'.format(dias_despesas))


with engine_dw.connect() as con:
    result = con.execute(
        "DELETE FROM fato_despesas d where d.cd_tempo >= date_format(last_day(curdate() - interval {} month) + interval 1 day,'%%Y%%m')".format(dias_despesas))
print('deletado com sucesso.')
print('Valores sem cd_matricula:')
print(df_fato_despesa[df_fato_despesa['CD_MATRICULA'].isnull()][{'CD_TEMPO','CD_PRESTADOR','VL_TOTAL_DESPESAS'}].to_string(index=False))

df_res_fato_despesas = df_fato_despesa[~df_fato_despesa['CD_MATRICULA'].isnull()]
print('executando o carregamento dos dados dos ultimos {} meses na tabela fato_despesas...'.format(dias_despesas))
df_res_fato_despesas.to_sql('fato_despesas', con=engine_dw, if_exists='append', index=False, dtype={"cd_tempo": INTEGER})
print('carregamento tabela fato_despesas finalizado.')

data_execucao = datetime.today().strftime('%d/%m/%Y %H:%M:00')
data = datetime.strptime(data_execucao,'%d/%m/%Y %H:%M:00')

with engine_dw.connect() as con:
    con.execute('''DELETE FROM historico_execucao H WHERE H.script = 'biprovedor' ''')
    con.execute('''INSERT INTO historico_execucao (data_execucao, script) VALUES ('{}', 'biprovedor')'''.format(data))
