sql_Dim_beneficiario = '''
SELECT Q.CD_MATRICULA                             AS "cd_matricula",
       Q.NM_SEGURADO                              AS "nm_beneficiario",
       CASE WHEN Q.NM_CIDADE IS NULL THEN 'NÃO INFORMADO' ELSE Q.NM_CIDADE END AS nm_cidade,
        case when C.TP_CONTRATO = 'I' THEN 'PF' ELSE 'PJ' END                   AS tipo_beneficiario,
       CASE
           WHEN Q.SN_TITULAR = 'N' THEN 'Dependente'
           WHEN Q.SN_TITULAR = 'S' THEN 'Titular'
           ELSE 'Fora da Regra' END               AS "titularidade",
       CASE
           WHEN Q.TP_SEXO = 'F' THEN 'Feminino'
           WHEN Q.TP_SEXO = 'M' THEN 'Masculino'
           ELSE 'FORA DA REGRA'  END              AS "sexo",

       case when(SELECT D.CD_MATRICULA FROM DBAPS.DESLIGAMENTO D
                   WHERE D.CD_MATRICULA = Q.CD_MATRICULA
                     AND D.DT_REATIVACAO IS NULL
           GROUP BY D.CD_MATRICULA) = Q.CD_MATRICULA then 'Desligado' else 'ativos' end  AS "status"
  FROM DBAPS.USUARIO Q
INNER JOIN DBAPS.CONTRATO C ON C.CD_CONTRATO = Q.CD_CONTRATO

ORDER BY 1 ASC
'''

sql_dim_prestador = '''
SELECT P.CD_PRESTADOR as cd_prestador,
       TP.NM_PRESTADOR AS tipo_prestador,
       CASE WHEN P.NM_PRESTADOR IS NULL THEN 'NÃO RASTREAVEL' ELSE P.NM_PRESTADOR END as nm_prestador,
       NVL((SELECT TRIM(PE.DS_MUNICIPIO)||'-'||TRIM(PE.CD_UF) FROM DBAPS.PRESTADOR_ENDERECO PE WHERE PE.CD_PRESTADOR = P.CD_PRESTADOR AND PE.SN_PRINCIPAL = 'S' AND rownum <= 1),'NÃO RASTREAVEL') AS "nm_cidade_prestador",
       CASE WHEN P.NM_UF IS NULL THEN 'NÃO RASTREAVEL' ELSE P.NM_UF END as nm_uf,
       P.TP_PRESTADOR as tp_prestador
    FROM DBAPS.PRESTADOR P
 INNER JOIN DBAPS.TIP_PRESTADOR TP ON TP.CD_TIP_PRESTADOR = P.CD_TIP_PRESTADOR
{}

'''

sql_fato_receitas = '''
SELECT
       M.NR_ANO||M.NR_MES AS cd_tempo,
       C.CD_CONTRATO as cd_contrato,
       CASE WHEN C.TP_CONTRATO = 'I' THEN 'PF' ELSE 'PJ' END                   AS tipo_contrato,
       sum(IU.VL_LANCAMENTO) vl_receita
 FROM DBAPS.MENS_CONTRATO M
 INNER JOIN DBAPS.MENS_USUARIO MU ON MU.CD_MENS_CONTRATO = M.CD_MENS_CONTRATO
 INNER JOIN DBAPS.ITMENS_USUARIO IU ON IU.CD_MENS_USUARIO = MU.CD_MENS_USUARIO
 LEFT JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = MU.CD_MATRICULA
 LEFT JOIN DBAPS.CONTRATO C ON C.CD_CONTRATO = M.CD_CONTRATO
 WHERE --M.NR_ANO || M.NR_MES = SUBSTR(('10/2020'), 4, 7) || SUBSTR(('10/2020'), 0, 2) AND
         M.CD_MOTIVO_CANCELAMENTO IS NULL
         AND IU.CD_LCTO_MENSALIDADE = 1
    {}
GROUP BY M.NR_ANO||M.NR_MES,C.CD_CONTRATO,CASE WHEN C.TP_CONTRATO = 'I' THEN 'PF' ELSE 'PJ' END
ORDER BY M.NR_ANO||M.NR_MES desc
'''

sql_fato_despesas ='''
SELECT x.cd_tempo as cd_tempo,
       x.cd_matricula as cd_matricula,
       x.cd_prestador as cd_prestador,
       sum(x.vl_copart) as vl_copart,
       sum(x.vl_total_despesas) as vl_total_despesas
       FROM (
-- FATOR SINISTRO SADT
 SELECT V.DT_COMPETENCIA as cd_tempo,
        V.cd_matricula as cd_matricula,
        V.CD_PRESTADOR_PRINCIPAL as cd_prestador,
        V.vl_total_franquia as vl_copart,
        0 as vl_total_despesas
 FROM DBAPS.V_CTAS_MEDICAS V
 WHERE NVL(V.vl_total_pago, 0) <> 0
   AND V.TP_SITUACAO <> 'NA'
   AND V.vl_total_franquia <> 0
   AND V.DT_COMPETENCIA >= to_char(last_day(add_months(sysdate, -{})) + 1,'yyyymm')
   --AND V.dt_competencia = SUBSTR(('10/2020'), 4, 7) || SUBSTR(('10/2020'), 0, 2)
UNION ALL
 -- FATOR SINISTRO INTERNACAO
 SELECT F.NR_ANO||F.NR_MES as cd_tempo,
        H.CD_USUARIO as cd_matricula,
        H.CD_PRESTADOR as cd_prestador,
        H.VL_FRANQUIA vl_copart,
        0 as vl_total_despesas
 FROM DBAPS.CONTA_HOSPITALAR H
        INNER JOIN DBAPS.LOTE L ON L.CD_LOTE = H.CD_LOTE
        INNER JOIN DBAPS.FATURA F ON F.CD_FATURA = L.CD_FATURA
        INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = H.CD_USUARIO
 WHERE   F.NR_ANO||F.NR_MES >= to_char(last_day(add_months(sysdate, -{})) + 1,'yyyymm')
         --F.NR_ANO||f.NR_MES = SUBSTR(('10/2020'), 4, 7) || SUBSTR(('10/2020'), 0, 2)
         AND H.CD_MENS_CONTRATO <> 1
         AND NVL(H.VL_FRANQUIA, 0) <> 0
--- DESPESAS LIQUIDAS
UNION ALL
 SELECT V.DT_COMPETENCIA as cd_tempo,
        V.cd_matricula as cd_matricula,
        V.CD_PRESTADOR_PRINCIPAL as cd_prestador,
        0 as vl_copart,
        V.vl_total_pago vl_total_despesas
 FROM DBAPS.V_CTAS_MEDICAS V
     LEFT JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = V.CD_MATRICULA
     LEFT JOIN DBAPS.PRESTADOR P ON P.CD_PRESTADOR = V.cd_prestador_pagamento
     INNER JOIN DBAPS.REPASSE_PRESTADOR RP ON RP.CD_REPASSE_PRESTADOR = V.CD_REPASSE_PRESTADOR
 WHERE 
      V.TP_SITUACAO_CONTA IN ('AA', 'AT')
    AND V.TP_SITUACAO_ITCONTA IN ('AA', 'AT')
    AND V.TP_SITUACAO_EQUIPE IN ('AA', 'AT')
    AND V.TP_ORIGEM = '2'
    AND RP.CD_CON_PAG IS NOT NULL
   AND V.DT_COMPETENCIA >= to_char(last_day(add_months(sysdate, -{})) + 1,'yyyymm')
   AND NVL(V.vl_total_pago, 0) > 0
   --AND V.dt_competencia = SUBSTR(('10/2020'), 4, 7) || SUBSTR(('10/2020'), 0, 2)
    ) X
GROUP BY X.cd_tempo, x.cd_matricula, x.cd_prestador
'''

sql_fato_beneficiario = '''
SELECT substr(BI.ANO_MES,4,8)||substr(BI.ANO_MES,1,2) AS CD_TEMPO,
 CASE
 WHEN TO_CHAR(BI.PRIMEIRO_DIA_MES,'MM/YYYY') = TO_CHAR(SYSDATE,'MM/YYYY') THEN (SELECT COUNT(Q.CD_MATRICULA)
 FROM DBAPS.USUARIO Q
 WHERE Q.DT_CADASTRO <= SYSDATE
 AND Q.CD_MATRICULA NOT IN (-2)
 AND NOT EXISTS (SELECT 1
 FROM DBAPS.DESLIGAMENTO D
 WHERE D.CD_MATRICULA = Q.CD_MATRICULA
 AND D.DT_REATIVACAO IS NULL
 AND D.DT_DESLIGAMENTO <= SYSDATE))
 ELSE (SELECT COUNT(Q.CD_MATRICULA)
 FROM DBAPS.USUARIO Q
 WHERE Q.DT_CADASTRO <= LAST_DAY(BI.PRIMEIRO_DIA_MES)
 AND Q.CD_MATRICULA NOT IN (-2)
 AND NOT EXISTS (SELECT 1 FROM DBAPS.DESLIGAMENTO D
 WHERE D.CD_MATRICULA = Q.CD_MATRICULA
 AND D.DT_REATIVACAO IS NULL
 AND D.DT_DESLIGAMENTO <= LAST_DAY(LAST_DAY(BI.PRIMEIRO_DIA_MES)))) END AS "NUMERO_VIDAS"
 FROM DBAPS.CALENDARIO_BI BI
 WHERE BI.PRIMEIRO_DIA_MES >= TO_DATE('2018-01-01','YY-MM-DD')
 AND BI.PRIMEIRO_DIA_MES <= TO_DATE( TO_CHAR(SYSDATE,'MM/YYYY'),'MM/YYYY' )
ORDER BY BI.PRIMEIRO_DIA_MES ASC
'''


sql_fato_valores_mes = '''
select X.cd_tempo               AS cd_tempo,
       SUM(X.qtd_beneficiarios) AS qtd_beneficiarios,
       SUM(X.receita_mes)       AS receita_mes,
       SUM(X.despesa_mes)       AS despesa_mes,
       SUM(X.vl_copart) AS vl_copart

from (
     SELECT substr(BI.ANO_MES, 4, 8) || substr(BI.ANO_MES, 1, 2)                                           AS cd_tempo,
            CASE
                WHEN TO_CHAR(BI.PRIMEIRO_DIA_MES, 'MM/YYYY') = TO_CHAR(SYSDATE, 'MM/YYYY')
                    THEN (SELECT COUNT(Q.CD_MATRICULA)
                          FROM DBAPS.USUARIO Q
                          WHERE Q.DT_CADASTRO <= SYSDATE
                            AND Q.CD_MATRICULA NOT IN (-2)
                            AND NOT EXISTS(SELECT 1
                                           FROM DBAPS.DESLIGAMENTO D
                                           WHERE D.CD_MATRICULA = Q.CD_MATRICULA
                                             AND D.DT_REATIVACAO IS NULL
                                             AND D.DT_DESLIGAMENTO <= SYSDATE))
                ELSE (SELECT COUNT(Q.CD_MATRICULA)
                      FROM DBAPS.USUARIO Q
                      WHERE Q.DT_CADASTRO <= LAST_DAY(BI.PRIMEIRO_DIA_MES)
                        AND Q.CD_MATRICULA NOT IN (-2)
                        AND NOT EXISTS(SELECT 1
                                       FROM DBAPS.DESLIGAMENTO D
                                       WHERE D.CD_MATRICULA = Q.CD_MATRICULA
                                         AND D.DT_REATIVACAO IS NULL
                                         AND D.DT_DESLIGAMENTO <= LAST_DAY(LAST_DAY(BI.PRIMEIRO_DIA_MES)))) END AS qtd_beneficiarios,
            0                                                                                                   as receita_mes,
            0                                                                                                   as despesa_mes,
            0 as vl_copart
     FROM DBAPS.CALENDARIO_BI BI
     WHERE BI.ANO || BI.MES = '{}'

         union all

         SELECT MC.NR_ANO || MC.NR_MES    as cd_tempo,
                0                         as qtd_beneficiarios,
                SUM(MC.VL_PAGO) AS receita_mes,
                0                         as despesa_mes,
                0 as vl_copart
         FROM DBAPS.CONTRATO C
                  INNER JOIN DBAPS.MENS_CONTRATO MC ON MC.CD_CONTRATO = C.CD_CONTRATO
         WHERE MC.NR_ANO || MC.NR_MES = '{}'
         group by MC.NR_ANO || MC.NR_MES

         union all

         SELECT A.DT_COMPETENCIA               as cd_tempo,
                0                              as qtd_beneficiarios,
                0                              as receita_mes,
                SUM(ROUND(A.VL_TOTAL_PAGO, 2)) AS despesa_mes,
                0 as vl_copart
         FROM DBAPS.V_CTAS_MEDICAS A
                  LEFT JOIN DBAPS.PRESTADOR B ON A.CD_PRESTADOR_PRINCIPAL = B.CD_PRESTADOR
                  LEFT JOIN DBAPS.GUIA C ON A.NR_GUIA = C.NR_GUIA
                  LEFT JOIN DBAPS.PRESTADOR_ENDERECO D ON C.CD_PRESTADOR_ENDERECO = D.CD_PRESTADOR_ENDERECO
                  LEFT JOIN DBAPS.ESPECIALIDADE E ON C.CD_ESPECIALIDADE = E.CD_ESPECIALIDADE
                  LEFT JOIN DBAPS.PROCEDIMENTO F ON A.CD_PROCEDIMENTO = F.CD_PROCEDIMENTO
                  INNER JOIN DBAPS.REPASSE_PRESTADOR RP ON RP.CD_REPASSE_PRESTADOR = A.CD_REPASSE_PRESTADOR
                  LEFT JOIN DBAPS.PRESTADOR P ON P.CD_PRESTADOR = C.CD_PRESTADOR_EXECUTOR
             AND RP.CD_PRESTADOR = A.CD_PRESTADOR_PAGAMENTO
                  INNER JOIN DBAPS.PAGAMENTO_PRESTADOR PP ON RP.CD_PAGAMENTO_PRESTADOR = PP.CD_PAGAMENTO_PRESTADOR
                  INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = A.CD_MATRICULA
         WHERE A.TP_SITUACAO_CONTA IN ('AA', 'AT')
           AND A.TP_SITUACAO_ITCONTA IN ('AA', 'AT')
           AND A.TP_SITUACAO_EQUIPE IN ('AA', 'AT')
           AND A.TP_ORIGEM = '2'
           AND RP.CD_CON_PAG IS NOT NULL
           AND A.DT_COMPETENCIA = '{}'
           AND A.VL_TOTAL_PAGO > 0
         GROUP BY A.DT_COMPETENCIA

    union all

     SELECT V.DT_COMPETENCIA as cd_tempo,
            0                as qtd_beneficiarios,
            0                AS receita_mes,
            0                as despesa_mes,
            SUM(V.vl_total_franquia) AS "vl_copart"

     FROM DBAPS.V_CTAS_MEDICAS V
     WHERE NVL(V.vl_total_pago, 0) <> 0
       AND V.TP_SITUACAO <> 'NA'
       AND V.vl_total_franquia <> 0
       AND V.DT_COMPETENCIA = '{}'
     group by V.DT_COMPETENCIA
     UNION ALL
     -- FATOR SINISTRO INTERNACAO
     SELECT F.NR_ANO || F.NR_MES as cd_tempo,
            0 as               qtd_beneficiarios,
            0 AS               receita_mes,
            0 as               despesa_mes,
            sum(H.VL_FRANQUIA) AS vl_copart

     FROM DBAPS.CONTA_HOSPITALAR H
              INNER JOIN DBAPS.LOTE L ON L.CD_LOTE = H.CD_LOTE
              INNER JOIN DBAPS.FATURA F ON F.CD_FATURA = L.CD_FATURA
              INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = H.CD_USUARIO
     WHERE F.NR_ANO || F.NR_MES = '{}'

       AND H.CD_MENS_CONTRATO <> 1
       AND NVL(H.VL_FRANQUIA, 0) <> 0
     group by F.NR_ANO || F.NR_MES

    ) X
GROUP BY X.cd_tempo

'''




sql_vendas = '''
SELECT X.cd_tempo,
       X.CLASSIFICACAO,
       COUNT(X.CD_CONTRATO) as QTD_CONTRATOS,
       SUM(X.NRO_VIDAS) AS QUANTIDADE_BENEFICIARIOS
FROM (
SELECT TO_CHAR(C.DT_VENDA,'YYYYMM') as cd_tempo,
       C.CD_CONTRATO,
       C.TP_CONTRATO,
       (SELECT COUNT(U.CD_MATRICULA)
        FROM DBAPS.USUARIO U
        WHERE U.CD_CONTRATO = C.CD_CONTRATO
          AND U.DT_CADASTRO <=
              LAST_DAY(TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY'))
          AND U.SN_ATIVO = 'S') AS "NRO_VIDAS",

       CASE
           WHEN C.TP_CONTRATO = 'I' THEN 'PF'
           WHEN C.TP_CONTRATO = 'A' THEN 'ADESAO'
           WHEN C.TP_CONTRATO = 'E' AND (SELECT COUNT(U.CD_MATRICULA)
                                         FROM DBAPS.USUARIO U
                                         WHERE U.CD_CONTRATO = C.CD_CONTRATO
                                           AND U.DT_CADASTRO >= TO_DATE(TO_CHAR(
                                                                                '01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)),
                                                                        'DD/MM/YYYY')
                                           AND U.DT_CADASTRO <= LAST_DAY(TO_DATE(TO_CHAR(
                                                                                         '01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)),
                                                                                 'DD/MM/YYYY'))) >= 30 THEN 'PJ'
           ELSE 'PME'
           END                     "CLASSIFICACAO"
FROM DBAPS.CONTRATO C
WHERE C.DT_VENDA >= TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY')
  AND C.DT_VENDA <=
      LAST_DAY(TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY'))
  AND C.SN_DEMITIDO_APOSENTADO_OBITO <> 'S'
  AND (SELECT COUNT(U.CD_MATRICULA)
       FROM DBAPS.USUARIO U
       WHERE U.CD_CONTRATO = C.CD_CONTRATO
         AND U.DT_CADASTRO >= TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY')
         AND U.DT_CADASTRO <= LAST_DAY(TO_DATE(TO_CHAR('01' || SUBSTR(('{0}'), 0, 2) || SUBSTR(('{0}'), 4, 7)), 'DD/MM/YYYY'))) <> 0) X
GROUP BY X.cd_tempo, X.CLASSIFICACAO

'''