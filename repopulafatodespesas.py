from functions import *
from querys import *
from sqlalchemy.types import INTEGER
from datetime import datetime
from tqdm import tqdm
import time

# %% PARAMENTROS DE CONEXÕES AOS BANCO DE DADOS
user_dw, psw_dw, host_dw = get_paramentros_con('prd_db_provedor.db')
user_oracle, psw_oracle, host_oracle = get_paramentros_con('prdme.db')

# %%CONEXÃO DO BANCO MYSQL ONDE ESTA O DW
engine_dw = get_engine_dw()

sql_fato_despesas = '''
SELECT x.cd_tempo as cd_tempo,
       x.cd_matricula as cd_matricula,
       x.cd_prestador as cd_prestador,
       sum(x.vl_copart) as vl_copart,
       sum(x.vl_total_despesas) as vl_total_despesas
       FROM (
-- FATOR SINISTRO SADT
 SELECT V.DT_COMPETENCIA as cd_tempo,
        V.cd_matricula as cd_matricula,
        V.CD_PRESTADOR_PRINCIPAL as cd_prestador,
        V.vl_total_franquia as vl_copart,
        0 as vl_total_despesas
 FROM DBAPS.V_CTAS_MEDICAS V
 WHERE NVL(V.vl_total_pago, 0) <> 0
   AND V.TP_SITUACAO <> 'NA'
   AND V.vl_total_franquia <> 0
   AND V.dt_competencia = '{0}'
  
UNION ALL
 -- FATOR SINISTRO INTERNACAO
 SELECT F.NR_ANO||F.NR_MES as cd_tempo,
        H.CD_USUARIO as cd_matricula,
        H.CD_PRESTADOR as cd_prestador,
        H.VL_FRANQUIA vl_copart,
        0 as vl_total_despesas
 FROM DBAPS.CONTA_HOSPITALAR H
        INNER JOIN DBAPS.LOTE L ON L.CD_LOTE = H.CD_LOTE
        INNER JOIN DBAPS.FATURA F ON F.CD_FATURA = L.CD_FATURA
        INNER JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = H.CD_USUARIO
 WHERE   
         F.NR_ANO||f.NR_MES = '{0}'       
         AND H.CD_MENS_CONTRATO <> 1
         AND NVL(H.VL_FRANQUIA, 0) <> 0
--- DESPESAS LIQUIDAS
UNION ALL
 SELECT V.DT_COMPETENCIA as cd_tempo,
        V.cd_matricula as cd_matricula,
        V.CD_PRESTADOR_PRINCIPAL as cd_prestador,
        0 as vl_copart,
        V.vl_total_pago vl_total_despesas
 FROM DBAPS.V_CTAS_MEDICAS V
     LEFT JOIN DBAPS.USUARIO U ON U.CD_MATRICULA = V.CD_MATRICULA
     LEFT JOIN DBAPS.PRESTADOR P ON P.CD_PRESTADOR = V.cd_prestador_pagamento
     INNER JOIN DBAPS.REPASSE_PRESTADOR RP ON RP.CD_REPASSE_PRESTADOR = V.CD_REPASSE_PRESTADOR
 WHERE 
      V.TP_SITUACAO_CONTA IN ('AA', 'AT')
    AND V.TP_SITUACAO_ITCONTA IN ('AA', 'AT')
    AND V.TP_SITUACAO_EQUIPE IN ('AA', 'AT')
    AND V.TP_ORIGEM = '2'
    AND RP.CD_CON_PAG IS NOT NULL
   AND NVL(V.vl_total_pago, 0) > 0
   AND V.dt_competencia ='{0}'
     
    ) X
GROUP BY X.cd_tempo, x.cd_matricula, x.cd_prestador
'''

with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as con:
    meses = pd.read_sql(''' select C.ANO_MES,C.ANO||C.MES AS ANOMES
    from dbaps.CALENDARIO_BI C WHERE C.ANO||C.MES BETWEEN 201901 AND TO_CHAR(CURRENT_DATE,'YYYYMM')''', con)

for i in tqdm(range(len(meses))):
    competencia = meses.iloc[i]['ANOMES']
    with cx_Oracle.connect('{}/{}@{}'.format(user_oracle, psw_oracle, host_oracle)) as connection:
        df_fato_despesa = pd.read_sql(sql_fato_despesas.format(competencia), connection)

    df_res_fato_despesas = df_fato_despesa[~df_fato_despesa['CD_MATRICULA'].isnull()]
    df_res_fato_despesas.to_sql('fato_despesas', con=engine_dw, if_exists='append', index=False,
                            dtype={"cd_tempo": INTEGER})
print('carregamento tabela fato_despesas finalizado.')
